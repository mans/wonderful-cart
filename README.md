j'ai:

- crée une  application angular-cli
- ajouté un exemple app.component avec .spec.ts (tests unitaires)
- Lancé ng test, ng e2e et ng serve pour s'assurer que cela fonctionne sur ma machine (deployement en local)
- deployé sur "surge.sh" (site de deployement pour applications front-end)
- Configuré le déploiement sur  gitlab et mis en en place CI/CD